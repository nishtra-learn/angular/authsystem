import { Component, OnInit } from '@angular/core';
import { AuthService } from '../shared/auth.service';

@Component({
  selector: 'app-credentials-form',
  templateUrl: './credentials-form.component.html',
  styleUrls: ['./credentials-form.component.css']
})
export class CredentialsFormComponent implements OnInit {
  username: string;
  password: string;
  serverUrl: string;
  responseMessage: string;
  gotResponse: boolean;

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
  }

  submitCredentials() {
    if (this.username?.length < 3 || this.password?.length < 3)
      return;
    
    this.gotResponse = false;

    this.authService
      .sendCredentials(this.username, this.password, this.serverUrl)
      .subscribe((data) => {
        console.log(data);
        this.responseMessage = data['response'];
        this.gotResponse = true;
      });
  }

}
