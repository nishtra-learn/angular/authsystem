import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  sendCredentials(username: string, password: string, serverUrl?: string) {
    let url = serverUrl == null ? 'localhost:3000/auth' : serverUrl + '/auth';
    return this.http.post(url, { username, password });
  }
}
