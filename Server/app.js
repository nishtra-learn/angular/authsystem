const express = require("express");
const cors = require("cors");
const jsonParser = express.json();
const fs = require("fs");
const logFilename = "log.json";

const app = express();
// CORS config
// app.use(function (req, res, next) {
//     res.header("Access-Control-Allow-Origin", "*");
//     res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//     res.header("Access-Control-Allow-Methods", "GET, PATCH, PUT, POST, DELETE, OPTIONS");
//     next();
// });
app.use(cors());

app.post("/auth", jsonParser, (req, res) => {
    if (!req.body)
        return res.sendStatus(400);

    processCredentials(req, res);
});

app.listen(3000, () => {
    console.log("Server is up");
});


function processCredentials(req, res) {
    fs.readFile(logFilename, (error, data) => {
        if (error) {
            console.log("Log file not found!");
            throw new Error("Log file not found!");
        }
        else {
            let logObj = JSON.parse(data);

            let username = req.body["username"];
            let password = req.body["password"];

            if (username == undefined || password == undefined) {
                return res.sendStatus(400)
            }

            let existingEntry = logObj.find(t => t.username === username && t.password === password)
            if (existingEntry) {
                res.send({ response: "This user is already logged" });
            }
            else {
                let logEntry = {
                    datetime: new Date(),
                    username: username,
                    password: password
                }

                logObj.push(logEntry);
                log = JSON.stringify(logObj, null, 4);

                fs.writeFile(logFilename, log, () => {
                    res.send({ response: "User credentials were added to log file" });
                })
            }
        }
    });
}